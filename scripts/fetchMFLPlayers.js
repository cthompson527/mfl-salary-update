const fetch = require('node-fetch');

const fetchPlayers = async year => {
  const mflUrl = `https://api.myfantasyleague.com/${year}/export?TYPE=players&JSON=1`;
  const mflPlayersPromise = await fetch(mflUrl);
  const mflResponse = await mflPlayersPromise.json();
  return mflResponse.players.player;
};

module.exports = fetchPlayers;

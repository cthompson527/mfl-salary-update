const rp = require('request-promise');
const cheerio = require('cheerio');
const fs = require('fs');

// mapping from spotrac url team name to MFL team abbreviation
const teams = [
  { url: 'arizona-cardinals', abbr: 'ARI' },
  { url: 'atlanta-falcons', abbr: 'ATL' },
  { url: 'baltimore-ravens', abbr: 'BAL' },
  { url: 'buffalo-bills', abbr: 'BUF' },
  { url: 'carolina-panthers', abbr: 'CAR' },
  { url: 'chicago-bears', abbr: 'CHI' },
  { url: 'cincinnati-bengals', abbr: 'CIN' },
  { url: 'cleveland-browns', abbr: 'CLE' },
  { url: 'dallas-cowboys', abbr: 'DAL' },
  { url: 'denver-broncos', abbr: 'DEN' },
  { url: 'detroit-lions', abbr: 'DET' },
  { url: 'green-bay-packers', abbr: 'GBP' },
  { url: 'houston-texans', abbr: 'HOU' },
  { url: 'indianapolis-colts', abbr: 'IND' },
  { url: 'jacksonville-jaguars', abbr: 'JAC' },
  { url: 'kansas-city-chiefs', abbr: 'KCC' },
  { url: 'los-angeles-chargers', abbr: 'LAC' },
  { url: 'los-angeles-rams', abbr: 'LAR' },
  { url: 'miami-dolphins', abbr: 'MIA' },
  { url: 'minnesota-vikings', abbr: 'MIN' },
  { url: 'new-england-patriots', abbr: 'NEP' },
  { url: 'new-orleans-saints', abbr: 'NOS' },
  { url: 'new-york-giants', abbr: 'NYG' },
  { url: 'new-york-jets', abbr: 'NYJ' },
  { url: 'oakland-raiders', abbr: 'OAK' },
  { url: 'philadelphia-eagles', abbr: 'PHI' },
  { url: 'pittsburgh-steelers', abbr: 'PIT' },
  { url: 'san-francisco-49ers', abbr: 'SFO' },
  { url: 'seattle-seahawks', abbr: 'SEA' },
  { url: 'tampa-bay-buccaneers', abbr: 'TBB' },
  { url: 'tennessee-titans', abbr: 'TEN' },
  { url: 'washington-redskins', abbr: 'WAS' },
];

const scrape = async () => {
  const players = {};
  for (const team of teams) {
    const url = `https://www.spotrac.com/nfl/${team.url}/cap`;
    const teamPage = await rp(url);
    const $ = cheerio.load(teamPage, { ignoreWhitespace: true });
    const playerInfo = $('.teams > .datatable:first-child > tbody > tr');
    players[team.abbr] = {};

    for (let i = 0; i < playerInfo.length; i++) {
      const columns = $(playerInfo[i]);
      const [
        lastName,
        fullName,
        pos,
        baseSal,
        signBonus,
        rosterBonus,
        optionBonus,
        workoutBonus,
        restructureBonus,
        misc,
        deadCap,
        capHit,
        capPercent,
      ] = columns
        .text()
        .replace(/(\t| )/g, '')
        .split('\n')
        .filter(v => v.length > 0);
      const firstName = fullName.substring(0, fullName.indexOf(lastName));
      const capStrParsed = parseInt(
        capHit
          .substr(1)
          .split(',')
          .join(''),
      );
      const cap = isNaN(capStrParsed) ? 0 : capStrParsed;

      if (!players[team.abbr][pos]) {
        players[team.abbr][pos] = [];
      }

      players[team.abbr][pos].push({ firstName, lastName, pos, capHit: cap });
    }
  }

  // create a JSON file relative to the project root directory
  const file = fs.createWriteStream('./data/salaries.json');
  file.write(JSON.stringify(players));
  file.end();
  return file;
};

module.exports = scrape;

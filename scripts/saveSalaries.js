const salaries = require('../data/salaries.json');
const fetchMFL = require('./fetchMFLPlayers');
const fs = require('fs');

const hammingDistanceCompare = (strA, strB, maxDiff) => {
  if (strA.includes(strB) || strB.includes(strA)) {
    return true;
  }

  if (strA.length !== strB.length) {
    return false;
  }

  let dist = 0;
  for (let i = 0; i < strA.length; i++) {
    if (strA.charAt(i) !== strB.charAt(i)) {
      dist = dist + 1;
    }
  }

  return dist <= maxDiff;
};

const getSalary = (team, position, id, name) => {
  const nameRegEx = /(\.| )/g;
  const [last, first] = name.split(', ');
  const playerOneFirst = first.toLowerCase().replace(nameRegEx, '');
  const playerOneLast = last.toLowerCase().replace(nameRegEx, '');
  const playersInPosition = salaries[team][position];
  const salaryInfo =
    playersInPosition &&
    playersInPosition.find(
      player =>
        hammingDistanceCompare(
          playerOneFirst,
          player.firstName.toLowerCase().replace(nameRegEx, ''),
          2,
        ) &&
        hammingDistanceCompare(
          playerOneLast,
          player.lastName.toLowerCase().replace(nameRegEx, ''),
          2,
        ),
    );

  if (salaryInfo) {
    return { id, salary: salaryInfo.capHit, team, position, name };
  }
};

const getSalaries = mflPlayers => {
  const mflP = mflPlayers.map(player => {
    const { position, team, name, id } = player;
    const salaryInfo = getSalary(team, position, id, name);
    if (salaryInfo) {
      return salaryInfo;
    }

    // begin really dumb way of doing this. This is only to check various alternative names to positions
    if (position === 'LB') {
      const salaryInfo = getSalary(team, 'ILB', id, name);
      if (salaryInfo) {
        return salaryInfo;
      }
      const salaryInfoOther = getSalary(team, 'OLB', id, name);
      if (salaryInfoOther) {
        return salaryInfoOther;
      }
      const salaryInfoDE = getSalary(team, 'DE', id, name);
      if (salaryInfoDE) {
        return salaryInfoDE;
      }
    }
    if (position === 'S') {
      const salaryInfo = getSalary(team, 'FS', id, name);
      if (salaryInfo) {
        return salaryInfo;
      }
      const salaryInfoOther = getSalary(team, 'SS', id, name);
      if (salaryInfoOther) {
        return salaryInfoOther;
      }
    }

    if (position === 'RB') {
      const salaryInfo = getSalary(team, 'FB', id, name);
      if (salaryInfo) {
        return salaryInfo;
      }
    }

    if (position === 'PK') {
      const salaryInfo = getSalary(team, 'K', id, name);
      if (salaryInfo) {
        return salaryInfo;
      }
    }
    if (position === 'PN') {
      const salaryInfo = getSalary(team, 'P', id, name);
      if (salaryInfo) {
        return salaryInfo;
      }
    }
    if (position === 'DE') {
      const salaryInfo = getSalary(team, 'LB', id, name);
      if (salaryInfo) {
        return salaryInfo;
      }
    }

    return { id, salary: 0, team, position, name };
  });

  const foundPlayers = mflP.filter(player => player !== undefined);
  const noKickers = foundPlayers.filter(
    player =>
      player.position !== 'PK' &&
      player.position !== 'PN' &&
      player.position !== 'K' &&
      player.position !== 'P',
  );
  return noKickers;
};

const saveSalaries = async (year, outFile) => {
  const mflPlayers = await fetchMFL(year);
  const mflFilter = mflPlayers.filter(
    player =>
      player.team &&
      player.team !== 'FA' &&
      player.position !== 'TMWR' &&
      player.position !== 'TMDL' &&
      player.position !== 'TMLB' &&
      player.position !== 'TMDB' &&
      player.position !== 'TMTE' &&
      player.position !== 'Def' &&
      player.position !== 'Off' &&
      player.position !== 'ST' &&
      player.position !== 'Coach' &&
      player.position !== 'TMQB' &&
      player.position !== 'TMPK' &&
      player.position !== 'TMPN' &&
      player.position !== 'TMRB',
  );
  const salaries = getSalaries(mflFilter);
  const file = fs.createWriteStream(outFile);
  for (const salary of salaries) {
    file.write(`${salary.name} ${salary.team} $${salary.salary}\n`);
  }
  file.end();
};

module.exports = saveSalaries;

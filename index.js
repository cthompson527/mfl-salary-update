const yargs = require('yargs')
  .usage('Usage: node index.js [options]')
  .example('node index.js --scrape', 'scrape spotrac')
  .example(
    'node index.js --write --year 2019',
    'query MFL players from 2019 and save salaries to file',
  )
  .boolean('s')
  .alias('s', 'scrape')
  .describe('s', 'scrape spotrac for player salaries')
  .string('y')
  .alias('y', 'year')
  .describe('y', 'year from MFL to query')
  .default('y', new Date().getFullYear())
  .string('o')
  .alias('o', 'outfile')
  .describe('o', 'output file to save to')
  .default('o', 'output.txt')
  .boolean('w')
  .alias('w', 'write')
  .describe('w', 'write out player names and salaries to a file')
  .epilog(
    'You must specify to either scrape or write (not both)! First the tool will run scrape to scrape all the salaries and pipe the result to a data file.' +
      'Then, you run write to write a file to import into MFL.',
  )
  .help('h')
  .alias('h', 'help');
const scrape = require('./scripts/scrape');
const saveSalaries = require('./scripts/saveSalaries');

if (!yargs.argv.scrape && !yargs.argv.write) {
  yargs.showHelp();
  return;
}

if (yargs.argv.scrape && yargs.argv.write) {
  yargs.showHelp();
  return;
}

const start = async () => {
  if (yargs.argv.scrape) {
    await scrape();
  }

  if (yargs.argv.write) {
    await saveSalaries(yargs.argv.year, yargs.argv.outfile);
  }
};

start();
